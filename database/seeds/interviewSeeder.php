<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class interviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
            'descripion' => Str::random(250),
            'interview_date' => '2020-07-10',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'descripion' => Str::random(250),
            'interview_date' => '2020-07-13',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        ]
        
    ); 
    }
}
