@extends('layouts.app')

@section('title')

@section('content')
        <h1>Create candidate</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "descripion">Interview descripion</label>
            <input type = "text" class="form-control" name = "descripion">
        </div>     
        <div class="form-group">
            <label for = "interview_date">Interview date</label>
            <input type = "text" class="form-control" name = "interview_date" placeholder = "YYYY-MM-DD">
        </div>
        <div class="form-group"> Select candidate
        <select name="candidate" class="form-control" >
                  @foreach($candidates as $candidate)
                      <option value="{{ $candidate->id}}">
                          {{ $candidate->name }}
                      </option>
                  @endforeach
         </select></div>
         

         <div class="form-group"> Select user
         <select name="user" class="form-control" >
                  @foreach($users as $user)
                      <option value="{{ $user->id}}" @if($user->id == $userName->id)
                            selected
                          @endif>
                          {{ $user->name }}
                      </option>
                  @endforeach
         </select>
         </div>
         
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>
@endsection
