@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div>
    <a href =  "{{url('/interviews/create')}}"> Add new interview</a>
</div>
@if(isset($interviews))
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Descripion</th><th>Interview date</th><th>Candidate name</th><th>User name</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td> {{$interview->descripion}}</td>
            <td>{{$interview->interview_date}}</td>
            <td>{{$interview->candidate->name}}</td>
            <td>{{$interview->user->name}}</td>                          
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
            <td>
                                                              
        </tr>
    @endforeach
</table>
@endif
@if(!isset($interviews))
<h1>You have no Interviews yet</h1>
@endif
@endsection

